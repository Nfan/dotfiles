#!/usr/bin/env bash
# Switching spaces
# update recent space and current one

# Application closed / hidden / unhidden
# Update all spaces

COLOR_BACKGROUND=0xf7121116
COLOR_FOREGROUND=0xfffefdfe
COLOR_ACCENT=0xff15bffd
COLOR_BACKGROUND_LIGHT=0xff342f33

get-icon-color() {
    local space=$1

    local icon_color=$COLOR_ACCENT
    local space_windows_number=$(yabai -m query --spaces --space $space | jq -cr '.windows | length')
    local windows=$(yabai -m query --windows --space $space )
    local windows_number=$(echo $windows | jq -cr 'length')

    # Yabai doesn't track fullscreen, non-native-zoom, windows such as fullscreen chrome / firefox
    # but it does list those windows in the space information
    # so this check is to detect if there is such window in space
    if [ $space_windows_number -eq $windows_number ]; then
        local visible_windows_number=$(echo $windows | jq -cr '[.[] | select(."is-minimized" | not)] | length')
        [[ $visible_windows_number == 0 ]] && icon_color=$COLOR_BACKGROUND_LIGHT
    fi
    echo $icon_color
}

space-switch() {
    local recent_space_id=$(yabai -m query --spaces --space recent | jq -cr '.index')
    local current_space_id=$(yabai -m query --spaces --space | jq -cr '.index')
    local recent_icon_color=$(get-icon-color $recent_space_id)

    sketchybar \
        --set "space.$recent_space_id" \
            background.drawing=off \
            icon.color="$recent_icon_color" \
        \
        --set "space.$current_space_id" \
            background.drawing=on \
            icon.color="$COLOR_BACKGROUND"
}

update-all-spaces() {
    echo "`date`: update-all-spaces" >> /usr/local/var/log/sketchybar/spaces.out.log
    local current_space_id=$(yabai -m query --spaces --space | jq -cr '.index')
    local spaces_num_windows_per_space=$(yabai -m query --spaces | jq -cr '[.[] | .index as $space | { "\($space)": (.windows | length) }] | reduce .[] as $item ({};  . + $item)')
    local windows_grouped_by_space=$(yabai -m query --windows | jq -cr 'group_by(.space)')
    local windows_num_windows_per_space=$(echo $windows_grouped_by_space | jq -cr '[.[] | .[0].space as $space | { "\($space)": length }] | reduce .[] as $item ({}; . + $item)')
    local windows_num_visible_windows_per_space=$(echo $windows_grouped_by_space | jq -cr '[.[] | [.[] | select(."is-minimized" | not)] | .[0].space as $space | { "\($space)": length }] | reduce .[] as $item ({}; . + $item)')

    local cmd="sketchybar "
    for space in {1..10}; do
        if [ $current_space_id -eq $space ]; then
            cmd="$cmd --set space.$space icon.color=$COLOR_BACKGROUND background.drawing=on"
            continue
        fi

        local spaces_num_wins=$(echo $spaces_num_windows_per_space | jq -cr .\"$space\")
        local windows_num_wins=$(echo $windows_num_windows_per_space | jq -cr .\"$space\")
        local windows_num_visible_wins=$(echo $windows_num_visible_windows_per_space | jq -cr .\"$space\")

        [ "$spaces_num_wins" == "$windows_num_wins" ] && local num_wins=$windows_num_visible_wins || local num_wins=$spaces_num_wins
        if [[ $num_wins == 0 ]] || [[ $num_wins == "null" ]]; then
            cmd="$cmd --set space.$space icon.color=$COLOR_BACKGROUND_LIGHT background.drawing=off"
        else
            cmd="$cmd --set space.$space icon.color=$COLOR_ACCENT background.drawing=off"
        fi
    done
    eval "$cmd"
}

case $1 in
    space_switch)
        space-switch
        ;;
    update_all_spaces)
        update-all-spaces
        ;;
    *)
        echo "sketchybar-space: unknown cmd $1"
        ;;
esac
