#!/bin/bash
# mpv "$FIRST" --external-file= --lavfi-complex='[vid1]scale=640:-1[v0];[v0][vid2]vstack=inputs=2 [vo]'

FIRST=$1
cmd="mpv --really-quiet \"$FIRST\""

if [ $# -eq 1 ]; then
  eval $cmd &>/dev/null &
  disown && exit 0
fi

shift
inputs=1
width=960

filter="[vid1]scale=$width:-1[v1]"
stack="[v1]"

for vid in "$@"; do
  ((inputs++))
  cmd="$cmd --external-file=\"$vid\""
  filter="$filter;[vid$inputs]scale=$width:-1[v$inputs]"
  stack="$stack[v$inputs]"
done

cmd="$cmd --lavfi-complex='$filter;$stack vstack=inputs=$inputs [vo]'"
eval $cmd &>/dev/null &
