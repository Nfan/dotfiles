local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        'git',
        'clone',
        '--filter=blob:none',
        'https://github.com/folke/lazy.nvim.git',
        '--branch=stable', -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    {
        'nvim-treesitter/nvim-treesitter',
        build = function()
            require("nvim-treesitter.install").update({ with_sync = true })()
        end,
    },
    {
        'nvim-telescope/telescope.nvim',
        branch = '0.1.x',
        dependencies = { 'nvim-lua/plenary.nvim' }
    },
    {
        'numToStr/Comment.nvim',
        dependencies = { 'JoosepAlviste/nvim-ts-context-commentstring' },
        lazy = false,
    },
    --
    'neovim/nvim-lspconfig',
    'L3MON4D3/LuaSnip',
    'mfussenegger/nvim-jdtls',
    -- Different useful stuff
    {
        'NvChad/nvim-colorizer.lua',
        config = function ()
            require('colorizer').setup {}
        end
    },
    {
        'nvim-tree/nvim-tree.lua', -- file tree
        version = '*',
        lazy = false,
        dependencies = { 'nvim-tree/nvim-web-devicons' }
    },
    'machakann/vim-sandwich',  -- operations on braces
    'tpope/vim-fugitive',
    'lewis6991/gitsigns.nvim',
    {
        'nvimdev/lspsaga.nvim',
        dependencies = {
            'nvim-treesitter/nvim-treesitter',
            'nvim-tree/nvim-web-devicons'
        },
    },
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' }
    },

    -- Completion
    'hrsh7th/nvim-cmp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'hrsh7th/cmp-nvim-lua',
    'hrsh7th/cmp-nvim-lsp',
    'saadparwaiz1/cmp_luasnip',
    'windwp/nvim-autopairs',

    'onsails/lspkind.nvim',  -- pictograms for LSP completion
    'lukas-reineke/cmp-under-comparator',

    -- Color schemes
    'folke/tokyonight.nvim',
    { "catppuccin/nvim", name = "catppuccin", priority = 1000 }

})
