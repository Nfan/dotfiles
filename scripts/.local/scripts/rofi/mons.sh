#!/usr/bin/env bash

ANS=$(echo "Extend|Duplicate|Mirror|Primary|Secondary" | \
  rofi -sep "|" -dmenu -i -p 'System' "" -width 20 \
  -hide-scrollbar -eh 1 -line-padding 4 -padding 20 -lines 5)
  case "$ANS" in
    *Extend) mons -e top;;
    *Duplicate) mons -d;;
    *Mirror) mons -m ;;
    *Primary) mons -o;;
    *Secondary) mons -s;;
  esac
pkill compton && compton --config ~/.config/compton/compton.conf -b --experimental-backends && nitrogen --restore
