#!/bin/bash
LOG_FILE=/tmp/nvidia-xrandr.log
xrandr --verbose --setprovideroutputsource modesetting NVIDIA-0 >> $LOG_FILE \
  && xrandr --verbose --auto >> $LOG_FILE
  # && xrandr --verbose --dpi 96 \
