# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=20000
SAVEHIST=20000
setopt autocd nomatch notify
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/nfan/.zshrc'
# End of lines configured by zsh-newuser-install
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'

# The following lines were added by compinstall
autoload -Uz compinit
compinit
# End of lines added by compinstall
setopt interactive_comments

# Shell plugins manager https://github.com/rossmacarthur/sheldon
eval "$(sheldon source)"
# Prompt line configuration
# eval "$(starship init zsh)"
autoload -Uz vcs_info
zstyle ':vcs_info:git:*' formats ' %b '
precmd_functions+=(vcs_info)

setopt prompt_subst
#export PROMPT='
#%K{white}%F{black} %D %* %K{green}%F{black} %m %K{yellow}%F{black}$vcs_info_msg_0_%K{cyan}%F{black} %~ %k%F{default}
#%F{cyan}>%F{default} '
export PROMPT='
%K{yellow}%F{black}$vcs_info_msg_0_%K{blue}%F{yellow}%K{blue}%F{black} %~ %k%F{blue}%k%F{default}
%F{blue}⟩%F{default} '


export RPROMPT=

[[ -n "$TMUX" ]] && export TERM="xterm-256color"

if [[ `uname` == 'Darwin' ]]; then
    source "$HOME/.cardcli_profile"
    source "$HOME/.cargo/env"
    source /usr/local/opt/asdf/libexec/asdf.sh
    # source /usr/local/opt/asdf/etc/bash_completion.d/asdf.bash
fi

sourceConfigFile() {
  filename="$1"
  [[ -f $XDG_CONFIG_HOME/shell/$filename ]] && source "$XDG_CONFIG_HOME/shell/$filename"
}
sourceConfigFile "aliases"
sourceConfigFile "functions"
source "$XDG_CONFIG_HOME/shell-work/amazon"

bindkey '^k' history-beginning-search-backward
bindkey '^j' history-beginning-search-forward
bindkey -s '^_' 'f=$(fd --type f | fzy) && nvim $f\015'
bindkey -s '^g' 'lfcd\015'
# show man page for a cunrrent cmd in stdin, without destroying whole cmd line
bindkey -s '^h' '^a\ed^y^a#^mman ^y^m^p^p^a^d^e'

source $HOME/.cardcli_profile
