local wezterm = require('wezterm')
local act = wezterm.action;

local is_linux = wezterm.home_dir:match('^/home') ~= nil

local config = wezterm.config_builder()
config.term = 'wezterm'
config.front_end = 'WebGpu'

config.window_decorations = "RESIZE"
config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0,
}

config.color_scheme = 'Catppuccin Mocha'
config.window_background_opacity = 0.97
config.use_fancy_tab_bar = false
config.hide_tab_bar_if_only_one_tab = true
config.tab_bar_at_bottom = true

config.font = wezterm.font_with_fallback {
  'JetBrains Mono NL',
  'Symbols Nerd Font Mono',
  'Noto Color Emoji',
  'Noto Sans Mono CJK JP',
}
config.font_size = 13.0
if is_linux then
  config.font_size = 11.0
end

config.audible_bell = 'Disabled'
config.force_reverse_video_cursor = false
config.foreground_text_hsb = {
  hue = 1.0,
  saturation = 1.1,
  brightness = 1.2,
}


config.keys = {
    -- {key="/", mods="CTRL", action=wezterm.action{SendString="\x1f"}},
    { key="g", mods="CTRL|ALT", action=act{QuickSelectArgs={
        label = "open URL",
        patterns = {
            "https?://\\S+"
        },
        action = wezterm.action_callback(function(window, pane)
            local url = window:get_selection_text_for_pane(pane)
            wezterm.open_with(url)
        end)
    }}},
    { key='l', mods='CTRL|ALT', action=act.ActivatePaneDirection('Right')},
    { key='h', mods='CTRL|ALT', action=act.ActivatePaneDirection('Left')},
    { key='j', mods='CTRL|ALT', action=act.ActivatePaneDirection('Down')},
    { key='k', mods='CTRL|ALT', action=act.ActivatePaneDirection('Up')},

    { key='t', mods='CTRL|ALT', action=act.SpawnTab 'CurrentPaneDomain' },
    { key='Tab', mods='CTRL|ALT', action=act.ActivateLastTab },

    { key='v', mods='CTRL|ALT', action=act.SplitHorizontal { domain = 'CurrentPaneDomain' } },
    { key='b', mods='CTRL|ALT', action=act.SplitVertical { domain = 'CurrentPaneDomain' } },

    { key='f', mods='CTRL|ALT', action=act.TogglePaneZoomState },
    { key='x', mods='CTRL|ALT', action=act.ActivateCopyMode },
}


for i = 1, 8 do
  -- CTRL+ALT + number to activate that tab
  table.insert(config.keys, {
    key = tostring(i),
    mods = 'CTRL|ALT',
    action = act.ActivateTab(i - 1),
  })
end


-- The filled in variant of the < symbol
local SOLID_LEFT_ARROW = wezterm.nerdfonts.ple_lower_right_triangle
-- The filled in variant of the > symbol
local SOLID_RIGHT_ARROW = wezterm.nerdfonts.ple_lower_right_triangle

-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
local function tab_title(tab_info)
  local title = tab_info.tab_title
  -- if the tab title is explicitly set, take that
  if title and #title > 0 then
    return title
  end
  -- Otherwise, use the title from the active pane
  -- in that tab
  return tab_info.active_pane.title
end

wezterm.on(
  'format-tab-title',
  function(tab, tabs, panes, conf, hover, max_width)
    local palette = conf.resolved_palette
    local edge_background = palette.background
    local background = palette.tab_bar.new_tab.bg_color
    local foreground = palette.tab_bar.new_tab.fg_color

    local blue = palette.brights[5]
    if tab.is_active then
      background = blue
      foreground = palette.background
    elseif hover then
      background = '#3b3052'
      foreground = '#909090'
    end

    local edge_foreground = background

    local title = tab_title(tab)

    -- ensure that the titles fit in the available space,
    -- and that we have room for the edges.
    title = wezterm.truncate_right(title, max_width - 2)

    return {
      { Background = { Color = edge_background } },
      { Foreground = { Color = edge_foreground } },
      { Text = SOLID_LEFT_ARROW },
      { Background = { Color = background } },
      { Foreground = { Color = foreground } },
      { Text = title },
      { Background = { Color = edge_foreground } },
      { Foreground = { Color = edge_background } },
      { Text = SOLID_RIGHT_ARROW },
    }
  end
)

wezterm.on('update-right-status', function(window, pane)
  -- "Wed Mar 3 08:14"
  local date = wezterm.strftime '%a %b %-d %H:%M '
  local proc_info = pane ~= nil and pane:get_foreground_process_info()
  local proc_cwd = proc_info ~= nil and proc_info.cwd or ''
  -- local proc_path = pane:get_title()

  window:set_right_status(wezterm.format {
    { Text = proc_cwd .. '    ' .. date .. '    ' .. wezterm.hostname() },
  })
end)

return config

--[[ {
    "ansi": [
        "#45475a",
        "#f38ba8",
        "#a6e3a1",
        "#f9e2af",
        "#89b4fa",
        "#f5c2e7",
        "#94e2d5",
        "#bac2de",
    ],
    "background": "#1e1e2e",
    "brights": [
        "#585b70",
        "#f38ba8",
        "#a6e3a1",
        "#f9e2af",
        "#89b4fa",
        "#f5c2e7",
        "#94e2d5",
        "#a6adc8",
    ],
    "compose_cursor": "#f2cdcd",
    "cursor_bg": "#f5e0dc",
    "cursor_border": "#f5e0dc",
    "cursor_fg": "#11111b",
    "foreground": "#cdd6f4",
    "indexed": {
        16: "#fab387",
        17: "#f5e0dc",
    },
    "scrollbar_thumb": "#585b70",
    "selection_bg": "#585b70",
    "selection_fg": "#cdd6f4",
    "split": "#6c7086",
    "tab_bar": {
        "active_tab": {
            "bg_color": "#cba6f7",
            "fg_color": "#11111b",
            "intensity": "Normal",
            "italic": false,
            "strikethrough": false,
            "underline": "None",
        },
        "background": "#11111b",
        "inactive_tab": {
            "bg_color": "#181825",
            "fg_color": "#cdd6f4",
            "intensity": "Normal",
            "italic": false,
            "strikethrough": false,
            "underline": "None",
        },
        "inactive_tab_edge": "#313244",
        "inactive_tab_hover": {
            "bg_color": "#1e1e2e",
            "fg_color": "#cdd6f4",
            "intensity": "Normal",
            "italic": false,
            "strikethrough": false,
            "underline": "None",
        },
        "new_tab": {
            "bg_color": "#313244",
            "fg_color": "#cdd6f4",
            "intensity": "Normal",
            "italic": false,
            "strikethrough": false,
            "underline": "None",
        },
        "new_tab_hover": {
            "bg_color": "#45475a",
            "fg_color": "#cdd6f4",
            "intensity": "Normal",
            "italic": false,
            "strikethrough": false,
            "underline": "None",
        },
    },
    "visual_bell": "#313244",
} ]]
