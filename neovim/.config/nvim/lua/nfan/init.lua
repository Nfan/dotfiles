require("nfan.options")   -- vim options config

require("nfan.lazy")    -- plugins management
require("nfan.plugins")   -- plugins configuration

require("nfan.autocmd")   -- general on vim event callbacks
require("nfan.color")     -- colorscheme and related configs
require("nfan.keymaps")
