#!/bin/bash

nvidia_modules=(nvidia nvidia-modeset nvidia-uvm nvidia-drm)
mods_remove() {
  for (( idx=${#nvidia_modules[@]}-1 ; idx>=0 ; idx-- )) ; do
    mod="${nvidia_modules[idx]}"
    echo "Removing $mod module"
    sudo rmmod $mod
  done
}
mods_add() {
  for mod in ${nvidia_modules[@]}; do
    echo "Loading $mod module"
    sudo modprobe $mod;
  done
}

driver_unbind() {
  sudo tee /sys/bus/pci/devices/0000\:01\:00.0/driver/unbind <<< "0000:01:00.0"
}
driver_bind() {
  driver=$1
  sudo tee /sys/bus/pci/drivers/$driver/driver_bind <<< "0000:01:00.0"
}

choose_vfio() {
  echo "Switching to VFIO driver";
  driver_unbind;
  sleep 1
  mods_remove;
  sleep 1
  bind vfio-pci;
}

choose_nvidia() {
  echo "Switching to NVIDIA driver"
  driver_unbind;
  # sleep 1
  # bind nvidia;
  sleep 2
  mods_add;
}

