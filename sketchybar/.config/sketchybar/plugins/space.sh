#!/usr/bin/env bash

# The $SELECTED variable is available for space components and indicates if
# the space invoking this script (with name: $NAME) is currently selected:
# https://felixkratz.github.io/SketchyBar/config/components#space----associate-mission-control-spaces-with-an-item

get-icon-color() {
    local space=$1
    local selected=$2

    local icon_color=$COLOR_BACKGROUND
    if [ "$selected" == "false" ]; then
        icon_color=$COLOR_FOREGROUND

        SPACE_WINDOWS_NUMBER=$(yabai -m query --spaces --space $space | jq -cr '.windows | length')
        WINDOWS=$(yabai -m query --windows --space $space )
        WINDOWS_NUMBER=$(echo $WINDOWS | jq -cr 'length')

        if [ $SPACE_WINDOWS_NUMBER -eq $WINDOWS_NUMBER ]; then
            VISIBLE_WINDOWS_NUMBER=$(echo $WINDOWS | jq -cr '[.[] | select(."is-minimized" | not)] | length')
            [ $VISIBLE_WINDOWS_NUMBER -eq 0 ] && icon_color=$COLOR_BACKGROUND_LIGHT
        fi
    fi
    echo $icon_color
}

ICON_COLOR=$(get-icon-color $SID "$SELECTED")

sketchybar --set $NAME  \
    background.drawing=$SELECTED \
    icon.color=$ICON_COLOR
