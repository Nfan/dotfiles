local signs = {
  Error = ' ',
  Warn = ' ',
  Info = ' ',
  Hint = ' ',
}
for type, icon in pairs(signs) do
  local hl = 'DiagnosticSign' .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

vim.diagnostic.config({
  signs = true,
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  virtual_text = {
    source = true,
  },
})

local is_work, a = pcall(require, 'nfan.work.a')
if is_work then
  a.setup_lsps()
end

local lspconfig = require('lspconfig')
local util = require('lspconfig.util')
local nfan_lsp = require('nfan.lsp')

local lsp_settings = {
  lua_ls = {
    settings = {
        Lua = {
          runtime = {
            -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
            version = 'LuaJIT',
          },
          diagnostics = {
            -- Get the language server to recognize the `vim` global
            globals = {'vim'},
          },
          workspace = {
            -- Make the server aware of Neovim runtime files
            library = vim.api.nvim_get_runtime_file("", true),
          },
          -- Do not send telemetry data containing a randomized but unique identifier
          telemetry = {
            enable = false,
          },
        },
    },
  },
  rust_analyzer = {
    ["rust-analyzer"] = {
      cargo = {
        features = "all",
      },
      checkOnSave = {
        command = "clippy",
      },
      procMacro = {
        enable = true,
      },
      diagnostics = {
        enable = true,
        disabled = { "unresolved-proc-macro" },
        experimental = {
          enable = true,
        }
      }
    }
  },
  tsserver = {},
  cssls = {},
  html = {},
  emmet_ls = {},
  dotls = {}, -- Graphviz dot files
  metals = {
      root_dir = util.root_pattern("build.sbt", "build.sc", "build.gradle", "pom.xml", "build.xml"),
  }, -- scala
  kotlin_language_server = {},
  sqlls = {},
  pyright = {},
  gdscript = {},
}

if is_work then
  lsp_settings['kata'] = {}
end

for lsp,lsp_config in pairs(lsp_settings) do
    local config = vim.tbl_deep_extend('force', {
        on_attach = nfan_lsp.on_attach,
        capabilities = nfan_lsp.capabilities,
    }, lsp_config)
    lspconfig[lsp].setup(config)
end
