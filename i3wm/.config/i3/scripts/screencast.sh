#!/bin/bash
FILE=~/Videos/screencast_$(date +%F_%H-%M-%S).mp4
if [ -z "$(pgrep ffcast)" ]; then
  # ffcast -q -# "$(xdotool getactivewindow)" rec -- -f pulse -i 0 -f pulse -i 1 -filter_complex amerge -ac 2 "$FILE"
  # ffcast -q rec -- -f pulse -i 0 -f pulse -i 1 -filter_complex amerge -ac 2 "$FILE" &
  ffcast -q rec -- -f pulse -i 3 "$FILE" &
else
  pkill ffmpeg
  notify-send "Screencast" "Saved to $FILE"
fi

pkill -RTMIN+14 i3blocks
