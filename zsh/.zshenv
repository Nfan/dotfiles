export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

unset LD_LIBRARY_PATH;

export XDG_CONFIG_HOME=$HOME/.config
export GNUPGHOME="$HOME/.local/share/gnupg"

export EDITOR="nvim"
export VISUAL="$EDITOR"
export SYSTEMD_EDITOR="$EDITOR"

export MOZ_ENABLE_WAYLAND=1
export MOZ_USE_XINPUT2=1   # Mozilla smooth scrolling/touchpads.
export BROWSER="firefox"

export GTK_THEME=Orchis-Dark
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc"
export QT_QPA_PLATFORMTHEME="qt5ct"
