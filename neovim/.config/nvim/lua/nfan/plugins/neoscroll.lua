local has_neoscroll, neoscroll = pcall(require, 'neoscroll')

if has_neoscroll then
    neoscroll.setup {
        easing_function = 'sine',
        cursor_scrolls_alone = true,
    }
end

