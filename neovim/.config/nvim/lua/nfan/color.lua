require('tokyonight').setup {
    style = 'moon',
    transparent = false,
    terminal_colors = true,
    styles = {
        comments = { italic = true },
        keywords = { italic = true },
        sidebars = 'dark',
        floats = 'dark',
    },
    sidebars =  { 'packer' },
    hide_inactive_statusline = false,
    on_highlights = function (highlight, colors)
        highlight.TabLineSel = {
            fg = colors.fg,
            bg = colors.bg_highlight
        }
        highlight.TabLine = {
            fg = colors.fg_dark,
            bg = colors.bg
        }
        highlight.TabLineFill = {
            fg = colors.fg_dark,
            bg = colors.bg,
        }
    end,
}
vim.opt.background = 'dark'
vim.cmd.colorscheme("catppuccin-macchiato")
