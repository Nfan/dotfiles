#!/bin/bash

usage() {
  echo "usage: screencast <display|region|window> <path/to/save/dir>";
}


if [ $# -lt 2 ]; then
  usage;
  exit 1;
fi;


screencast() {
  GEOMETRY="$1"
  SAVE_PATH="$2"
  wf-recorder -g "$GEOMETRY" -f "$SAVE_PATH" &
}

screencastOutput() {
  OUTPUT="$1"
  SAVE_PATH="$2"
  wf-recorder -o "$OUTPUT" -f "$SAVE_PATH"
}

TYPE=$1
SCREEN_DIR="$2";
SCREEN_PATH="$SCREEN_DIR"/screencast_$(date "+%F_%H-%M-%S_%N").mp4;

NOTIFY_TITLE=""

declare -r filter='
# returns the focused node by recursively traversing the node tree
def find_focused_node:
if .focused then . else (if .nodes then (.nodes | .[] | find_focused_node) else empty end) end;
  # returns a string in the format that grim expects
  def format_rect:
  "\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height)";
  find_focused_node | format_rect
  '

  if [ -z "$(pgrep wf-recorder)" ]; then
    case "$TYPE" in
      -h|--help)
        usage;
        exit 0;
        ;;
      region)
        screencast "$(slurp -d)" "$SCREEN_PATH";
        NOTIFY_TITLE="Region Screencast"
        ;;
      window)
        screencast "$(swaymsg --type get_tree --raw | jq --raw-output "${filter}")" "$SCREEN_PATH";
        NOTIFY_TITLE="Window Screencast"
        ;;
      *)
        screencastOutput "$(swaymsg --type get_outputs --raw | jq --raw-output '.[] | select(.focused) | .name')" "$SCREEN_PATH";
        NOTIFY_TITLE="Screencast"
        ;;
    esac;

  else
    pkill -INT wf-recorder;
    notify-send "Screencast" "Saved in $SCREEN_DIR"
  fi

  pkill -RTMIN+14 i3blocks

