#!/bin/bash
. $HOME/bin/gpu_functions.sh

pci_info="$(lspci -nnk -d 10de:1c8c)"
if [ -n "$pci_info" ]; then
  if [ -z "$(echo $pci_info | grep vfio)" ]; then
    choose_vfio;
  else
    choose_nvidia;
  fi
else
  echo "NVIDIA GPU is disabled"
fi
