local nnoremap = require('nfan.keymap').nnoremap
local nt_api = require('nvim-tree.api')

local change_root = function(node)
  local is_file = node.has_children == nil
  if is_file then
    nt_api.tree.change_root_to_node(node.parent)
  else
    nt_api.tree.change_root_to_node(node)
  end
end

require('nvim-tree').setup {
  view = {
    width = 50,
  },
  git = {
    ignore = false,
  },
  actions = {
    open_file = {
      quit_on_open = true,
    }
  },
  renderer = {
    group_empty = true,
  },
}

nnoremap('<C-n>', function()
  nt_api.tree.toggle()
end)

nnoremap('<leader><leader>n', function ()
  vim.cmd [[NvimTreeFindFile]]
end)

-- nt_api.tree.change_root_to_node
