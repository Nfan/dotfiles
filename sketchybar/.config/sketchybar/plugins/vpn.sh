#!/usr/bin/env sh

#82aaff
COLOR_BLUE="0xff82aaff"
#c3e88d
COLOR_GREEN="0xffc3e88d"
#ffc777
COLOR_YELLOW="0xffffc777"
#ff757f
COLOR_RED="0xffff757f"

CONNECTED_ICON=""
CONNECTED_COLOR="$COLOR_BLUE"

DISCONNECTED_ICON=""
DISCONNECTED_COLOR="$COLOR_RED"

if [[ `int-need-auth` == 0 ]]; then
    ICON=$CONNECTED_ICON
    COLOR=$CONNECTED_COLOR
else
    ICON=$DISCONNECTED_ICON
    COLOR=$DISCONNECTED_COLOR
fi

# The item invoking this script (name $NAME) will get its icon and label
sketchybar --set $NAME icon="$ICON" icon.color="$COLOR"
