local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

local my_group = augroup('Nfan', {})

-- remove trailing spaces
autocmd({'BufWritePre'}, {
  group = my_group,
  pattern = '*',
  command = "%s/\\s\\+$//e",
})

-- autoreload files if changed from outside
autocmd({ 'FocusGained', 'BufEnter' }, {
  group = my_group,
  pattern = '*',
  command = ':checktime',
})

-- override formatoptions with autocmd, because standard fieltype plugins always override it
autocmd({ 'FileType' }, {
    group = my_group,
    pattern = '*',
    callback = function ()
        vim.opt.formatoptions = vim.opt.formatoptions
        - 't' -- do not auto wrap using textwidth
        + 'c' -- auto wrap comments using textwidth
        + 'r' -- add comment leader on <Enter> in Insert mode
        - 'o' -- do not add comment leader on 'o' and 'O' in normal mode
        + 'q' -- format comments with gq
        - 'a' -- do not auto format main text (code)
        + 'n' -- indent bullet list item new line
        - '2' -- do not autoindent based on the second line of a paragraph
        - 'l' -- do not break long lines
        + '1' -- don't break line after a one-letter word, break before it instead
        + 'j' -- remove a comment leader on joining the lines when it makes sense
    end,
})

-- autosave
local isWritable = function()
  return vim.api.nvim_eval('filewritable(expand("%"))') == 1
end

autocmd({
    'FocusLost',
    'BufLeave',
    'SourcePre',    -- before :source
    'InsertLeave',
    'TextChanged',  -- text changed in Normal mode
  },
  {
    group = my_group,
    pattern = '*',
    callback = function()
      local buffer = 0
      if vim.o.modified and isWritable() then
        vim.api.nvim_buf_call(buffer, function ()
          vim.cmd.write { mods = { silent = true } }
          vim.api.nvim_exec_autocmds('BufWritePost', { buffer = buffer, modeline = false })
        end)
      end
    end,
  })

-- Sync packer after sourcing it
autocmd({ 'SourcePost' }, {
  group = my_group,
  pattern = 'packer.lua',
  command = ':PackerSync',
})

-- autobuild dot files
local job = require('plenary.job')
autocmd({'BufLeave', 'FocusLost', 'InsertLeave', 'TextChanged'}, {
  group = my_group,
  pattern = '*.dot',
  callback = function ()
    local filename = vim.api.nvim_buf_get_name(0)
    local graph_filename = filename:gsub('.dot', '.svg')
    job:new({
        command = 'dot',
        args = { '-Tsvg', filename, '-o'..graph_filename },
    }):start()
  end
})

