typeset -U path PATH

if [[ `uname` == 'Darwin' ]]; then
    unset JAVA_TOOL_OPTIONS
    path=("$HOME/Library/Application Support/Coursier/bin" $path)
    path=("$HOME/.rodar/bin" $path)
    path=("/usr/local/opt/coreutils/libexec/gnubin" $path)
fi

path=("$HOME/.local/bin" $path)
export PATH

if [[ `uname` != 'Darwin' ]] && [[ -z $DISPLAY ]]; then
    if [[ $(tty) = /dev/tty1 ]]; then
        exec sway
    elif [[ $(tty) = /dev/tty2 ]]; then
        startx-nvidia;
    fi
fi
