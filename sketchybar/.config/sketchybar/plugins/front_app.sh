#!/usr/bin/env sh

# Some events send additional information specific to the event in the $INFO
# variable. E.g. the front_app_switched event sends the name of the newly
# focused application in the $INFO variable:
# https://felixkratz.github.io/SketchyBar/config/events#events-and-scripting

# Split multistring by newline for an array
IFS=$'
'
YABAI_RESPONSE=($(yabai -m query --windows --window | jq -r '.app, .title'))
unset IFS
APP="${YABAI_RESPONSE[0]}"
TITLE="${YABAI_RESPONSE[1]}"
[ "$APP" == "$TITLE" ] && LABEL="$TITLE" || LABEL="$APP - $TITLE"
LABEL=${LABEL::100}
sketchybar --set $NAME label="$LABEL"
