# go to previous history command that starts with current input
bind '"\C-k": history-search-backward'
bind '"\C-j": history-search-forward'
# pop paused job
bind '"\C-f": "fg\015"'
# open man page for current command in input (Alt+h)
bind '"\eh": "\C-a\eb\ed\C-y\e#man \C-y\C-m\C-p\C-p\C-a\C-d\C-e"'
# search and edit a file
bind '"\C-_":"f=$(fd --type f | fzy) && nvim $f\015"'
bind '"\C-h": "lfcd\015"'

# cd just by path without cd command
shopt -s autocd
shopt -s checkwinsize
# typo cd
shopt -s cdspell

export GNUPGHOME="$HOME/.local/share/gnupg"
export TERMCMD="xterm"
export PATH="$HOME/.local/bin:$PATH"

HISTSIZE=20000
HISTFILESIZE=20000

export EDITOR="nvim"
export VISUAL="$EDITOR"
export SYSTEMD_EDITOR="$EDITOR"

export MOZ_ENABLE_WAYLAND=1
export MOZ_USE_XINPUT2="1"   # Mozilla smooth scrolling/touchpads.
export BROWSER="firefox"

# GUI theming
export GTK_THEME=Orchis-Dark
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc"
export QT_QPA_PLATFORMTHEME="qt5ct"

export XDG_CONFIG_HOME=$HOME/.config

sourceConfigFile() {
  filename="$1"
  if [ -f $XDG_CONFIG_HOME/shell/$filename ]; then
    . $XDG_CONFIG_HOME/shell/$filename;
  fi
}
sourceConfigFile aliases
sourceConfigFile functions

PS1='\[\033[0;32m\]\[\033[0m\033[0;32m\]\u\[\033[0;36m\]@\[\033[0;36m\]\h \w\[\033[0;32m\] $(ps1-git-branch)\[\033[0;32m\]\[\033[0m\033[0;32m\] $(ps1-vim-workspace)\n\$\[\033[0m\033[0;32m\]\[\033[0m\] '
set-terminal-title() {
    echo -ne "\033]0;$*\a"
}
trap 'set-terminal-title $BASH_COMMAND' DEBUG
PROMPT_COMMAND='set-terminal-title $PWD'


if [ -n "$TMUX" ]; then
  export TERM="xterm-256color";
fi;

tabs 4

export PATH="$HOME/.cargo/bin:$PATH"
# MAC
if [ `uname` == 'Darwin' ]; then
    unset JAVA_TOOL_OPTIONS
    export PATH=~/.rodar/bin:$PATH

    export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
    source $XDG_CONFIG_HOME/shell-work/amazon
    source /usr/local/opt/asdf/libexec/asdf.sh
    source /usr/local/opt/asdf/etc/bash_completion.d/asdf.bash
fi

