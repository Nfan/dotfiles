require('lspsaga').setup {
    finder_action_keys = {
        open = '<CR>',
        vsplit = '<C-v>',
        split = 'o',
        tabe = 't',
        quit = '<Esc>',
    },
    code_action_keys = {
        exec = '<CR>',
        quit = '<Esc>',
    },
    lightbulb = {
        enable = false,
        enable_in_insert = false,
        sign = false,
    },
    symbol_in_winbar = {
        enable = false,
    },
    outline = {
        win_width = 60,
    },
    callhierarchy = {
        show_detail = true,
    },
    finder = {
        max_height = 0.8,
    },
}

