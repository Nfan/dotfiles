#!/bin/bash

FILENAME="$1"
usage() {
  echo "usage: set-background <image/path>"
}
if [ -z "$FILENAME" ] || [ ! -e "$FILENAME" ]; then
  usage;
  exit 1;
fi;

BACKGROUND_PATH="$HOME/.local/share/background.jpg";
LOCKSCREEN_PATH="$HOME/.local/share/lockscreen.jpg";

setBackgroundLinux() {
  local img_fullpath="$1";
  ln -sf "$img_fullpath" "$BACKGROUND_PATH";
  swaymsg "output eDP-1 background \"$BACKGROUND_PATH\" fill";
}

setBackgroundMac() {
    local img_fullpath="$1"
    local current_space=$(yabai -m query --spaces --space | jq -cr '.index')

    for space in {1..10}; do
        yabai -m space --focus $space 
        osascript -e "tell application \"System Events\" to tell every desktop to set picture to \"$img_fullpath\""
    done
    yabai -m space --focus $current_space
}

setBackground() {
    if [ `uname` == "Darwin" ]; then
        setBackgroundMac "$1"
    else
        setBackgroundLinux "$1"
    fi
}

setLockscreen() {
  local img_fullpath="$1";
  ln -sf "$img_fullpath" "$LOCKSCREEN_PATH";
}

if [[ "$FILENAME" != /* ]]; then
  FILENAME="$PWD/$FILENAME";
fi;

select TYPE in Background Lockscreen; do
  case "$TYPE" in
    Background)
      setBackground "$FILENAME";
      break;
      ;;
    Lockscreen)
      setLockscreen "$FILENAME";
      break;
      ;;
  esac;
done;
