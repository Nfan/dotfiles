#!/usr/bin/env bash

DEVICE_ID="74-74-46-0e-d6-91"

[[ $(blueutil --is-connected $DEVICE_ID) == 0 ]] && blueutil --connect $DEVICE_ID || blueutil --disconnect $DEVICE_ID
