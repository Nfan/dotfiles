require('nvim-treesitter.configs').setup {
    ensure_installed = {
        'vimdoc',
        'comment',  -- comments highlighting, e.g. TODO, FIXME
        'java',
        'kotlin',
        'lua',
        'rust',
        'python',
        'javascript',
        'typescript',
        'tsx',
        'html',
        'css',
        'markdown',
        'markdown_inline',
        'make',
    },
    sync_install = false,
    auto_install = true,

    highlight = {
        enable = true,
    },

    indent = {
        enable = true,
    },

    context_commentstring = {
        enable = true,
        -- With Comment.nvim, we don't need to run this on the autocmd
        -- Only run in pre-hook
        enable_autocmd = false,
    }
}

