local is_work, a = pcall(require, 'nfan.work.a')
if not is_work then
  return
end

local nfan_lsp = require('nfan.lsp')

local home_path = os.getenv('HOME')
local lsp_files_path = home_path .. '/.local/share/jdtls/'
local lombok_path = lsp_files_path .. 'lombok.jar'
local root_path = require('jdtls.setup').find_root({ 'packageInfo' }, 'Config')

local project_name = vim.fn.fnamemodify(root_path, ':p:h:t')
local workspace_path = lsp_files_path .. project_name

local ws_folders_jdtls = a.get_ws_folders_jdtls(root_path)

-- See `:help vim.lsp.start_client` for an overview of the supported `config` options.
local config = {
  on_attach = nfan_lsp.on_attach,
  capabilities = nfan_lsp.capabilities,
  -- The command that starts the language server
  -- See: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line
  cmd = {
    'jdtls',
    '--jvm-arg=-javaagent:' .. lombok_path,
    '-data', workspace_path
  },

  -- This is the default if not provided, you can remove it. Or adjust as needed.
  -- One dedicated LSP server & client will be started per unique root_dir
  root_dir = root_path,

  -- Here you can configure eclipse.jdt.ls specific settings
  -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
  -- for a list of options
  -- settings = {
  --   java = {
  --     configuration = {
  --       runtimes = {
  --         {
  --           name = 'JavaSE-1_8',
  --           path = a.java_paths[8]
  --         },
  --         {
  --           name = 'JavaSE-18',
  --           path = a.java_paths[18]
  --         },
  --       },
  --     },
  --   },
  -- },

  -- Language server `initializationOptions`
  -- You need to extend the `bundles` with paths to jar files
  -- if you want to use additional eclipse.jdt.ls plugins.
  --
  -- See https://github.com/mfussenegger/nvim-jdtls#java-debug-installation
  --
  -- If you don't plan on using the debugger or other eclipse.jdt.ls plugins you can remove this
  init_options = {
    bundles = {},
    workspaceFolders = ws_folders_jdtls,
  },
}
-- This starts a new client & server,
-- or attaches to an existing client & server depending on the `root_dir`.
require('jdtls').start_or_attach(config)
