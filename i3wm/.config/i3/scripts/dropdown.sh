# if [ -z "$(pgrep -a termite | grep $NAME)" ]; then exec termite --name $NAME; fi

TERMINAL="$1"
NAME=dropdown

runTerminal() {
  $TERMINAL start --class $NAME;
}

(pgrep -a $TERMINAL | grep $NAME) && ( swaymsg "[app_id=$NAME] scratchpad show" || swaymsg "[app_id=$NAME] move scratchpad, scratchpad show" ) || runTerminal;
