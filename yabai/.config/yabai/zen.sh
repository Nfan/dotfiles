# https://github.com/donaldguy/dotfiles/blob/749c564f6b5f8f1d22ecc7f593cad5c8d80f8bd0/executable_dot_yabairc

function get_space_num_managed_windows {
 yabai -m query --windows --space mouse | \
   jq 'map(select(."is-visible" == true and ."is-floating" == false)) |
    # deal with how hidden applications apparently still
    # have "visible" windows
    if all(."split-type" == "none") then
      1
    else
      map(select(."split-type" != "none")) | length
    end'
}

function get_display_aspect_ratio {
  case $(yabai -m query --displays --display mouse | jq '.frame | (.w)/(.h) | floor') in
    (1) echo "standard" ;;
    (2) echo "wide" ;;
    (*) echo "something:whacky" ;;
  esac
}

function maybe_center_space {
  if [[ $(get_display_aspect_ratio) == "wide" ]]; then
    case $(get_space_num_managed_windows) in
      0 | 1)
        yabai -m space --padding abs:0:0:760:760
        ;;
      *)
        yabai -m space --padding abs:0:0:0:0
        ;;
    esac
  else
    yabai -m space --padding abs:0:0:0:0
  fi
}

#maybe_center_space
