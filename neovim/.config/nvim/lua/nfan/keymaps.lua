local nnoremap = require('nfan.keymap').nnoremap

nnoremap('<leader>/', vim.cmd.nohlsearch)
nnoremap('<leader><leader>s', '<cmd>source %<CR>')

-- windows navigation
nnoremap('<C-h>', '<C-w>h')
nnoremap('<C-j>', '<C-w>j')
nnoremap('<C-k>', '<C-w>k')
nnoremap('<C-l>', '<C-w>l')

--tabs navigation
nnoremap('<Tab>', 'gt')
nnoremap('<S-Tab>', 'gT')

-- Luasnip
local ls = require('luasnip')

vim.keymap.set( { 'i', 's' }, '<C-k>', function ()
  if ls.jumpable(1) then
    ls.jump(1)
  end
end, { silent = true })

vim.keymap.set( { 'i', 's' }, '<C-j>', function ()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end)

vim.keymap.set( { 'i' }, '<C-l>', function ()
  if ls.choice_active() then
    ls.change_choice()
  end
end)

-- Telescope
-- More, buffer and LSP specific, in plugins/lspconfig.lua
local telescope_builtin = require('telescope.builtin')
local leader = '<C-p>'
nnoremap(leader..'f', telescope_builtin.find_files)
nnoremap(leader..leader, telescope_builtin.live_grep)
nnoremap(leader..'g', telescope_builtin.git_status)
nnoremap(leader..'b', telescope_builtin.buffers)
nnoremap(leader..'c', telescope_builtin.commands)
nnoremap(leader..'h', telescope_builtin.help_tags)
nnoremap(leader..'m', telescope_builtin.marks)
nnoremap(leader..'z', telescope_builtin.colorscheme)
nnoremap(leader..'j', telescope_builtin.jumplist)
nnoremap(leader..'s', telescope_builtin.spell_suggest)
nnoremap(leader..'k', telescope_builtin.keymaps)
nnoremap(leader..'r', telescope_builtin.resume)
nnoremap(leader..'d', telescope_builtin.diagnostics)

nnoremap('n', 'nzz')
nnoremap('N', 'Nzz')

-- Documentation of useful keys built in nvim
-- <C-t> - move back in tag stack, tag stack is updated on each symbol definition jump instead of movement
-- Whole line completion, indent is ignored, more at :h ins-completion
-- <C-x><C-l> to add the next line after chosen
-- inoremap('<C-l>', '<C-x><C-l>')
