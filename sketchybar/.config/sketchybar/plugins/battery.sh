#!/usr/bin/env sh

PERCENTAGE=$(pmset -g batt | grep -Eo "\d+%" | cut -d% -f1)
CHARGING=$(pmset -g batt | grep 'AC Power')

#82aaff
COLOR_BLUE="0xff82aaff"
#c3e88d
COLOR_GREEN="0xffc3e88d"
#ffc777
COLOR_YELLOW="0xffffc777"
#ff757f
COLOR_RED="0xffff757f"

if [ $PERCENTAGE = "" ]; then
  exit 0
fi

if [ $PERCENTAGE -ge 80 ]; then
    ICON=""
    COLOR="$COLOR_BLUE"
elif [ $PERCENTAGE -ge 60 ]; then
    ICON=""
    COLOR="$COLOR_BLUE"
elif [ $PERCENTAGE -ge 40 ]; then
    ICON=""
    COLOR="$COLOR_GREEN"
elif [ $PERCENTAGE -ge 16 ]; then
    ICON=""
    COLOR="$COLOR_YELLOW"
else
    ICON=""
    COLOR="$COLOR_RED"
fi

if [[ $CHARGING != "" ]]; then
  ICON=""
  COLOR="0xff9ece6a"
fi

# The item invoking this script (name $NAME) will get its icon and label
# updated with the current battery status
sketchybar --set $NAME icon="$ICON" icon.color="$COLOR" label="${PERCENTAGE}%"

#7dcfff
#bb9af7
#f7768e
#e0af68
#9ece6a
