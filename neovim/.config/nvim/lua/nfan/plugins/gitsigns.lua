local gsa = require('gitsigns.actions')
local nnoremap = require('nfan.keymap').nnoremap

require('gitsigns').setup {
    -- word_diff = true,
}

nnoremap('<leader>gb', gsa.toggle_current_line_blame)
nnoremap('<leader>gd', gsa.diffthis)
nnoremap('<leader>go', gsa.show) -- show original file
nnoremap('<leader>gu', gsa.reset_hunk) -- show original file
nnoremap('<leader>gx', gsa.toggle_deleted)
nnoremap('<leader>gn', gsa.next_hunk)
nnoremap('<leader>gN', gsa.prev_hunk)

-- Better option:
-- 1. GIT_EDITOR=<some cmd that will retrieve passed COMMIT_EDITMSG location> git commit
-- 3. edit .git/COMMIT_EDITMSG
-- 4. execute on save: git commit -F .git/COMMIT_EDITMSG --cleanup=strip
nnoremap('<leader>gc', function ()
    vim.cmd [[new|setlocal signcolumn=no nonumber norelativenumber concealcursor=nc conceallevel=2 nomodeline foldmethod=manual|call termopen('git commit')|startinsert]]
end)
