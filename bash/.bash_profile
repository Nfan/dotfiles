export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi

unset LD_LIBRARY_PATH;

if [ `uname` == 'Darwin' ]; then
    [[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"
    source $HOME/.cardcli_profile
    . "$HOME/.cargo/env"
else
    if [[ -z $DISPLAY ]]; then
      if [[ $(tty) = /dev/tty1 ]]; then
        exec sway --unsupported-gpu
      elif [[ $(tty) = /dev/tty2 ]]; then
        startx-nvidia;
      fi
    fi
fi

