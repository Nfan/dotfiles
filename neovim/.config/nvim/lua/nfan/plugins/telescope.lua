require('telescope').setup {

    defaults = {
        -- right angle corners
        -- borderchars = {
        --     prompt = { '', '', '', '', '', '', '', ''},
        --     title = { '', '', '', '', '', '', '', ''},
        --     results = { '', '', '', '', '', '', '', ''},
        --     preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        -- },
        results_title = false,
        dynamic_preview_title = true,

        sorting_strategy = "ascending",
        layout_config = {
            horizontal = {
                height = 0.9,
                preview_cutoff = 150,
                preview_width = 0.7,
                width = 0.9,
                prompt_position = "top",
            }
        },
        -- entry_prefix = '•',
        -- selection_caret = '',
        -- wrap_results = true, -- wrap text of the results list items
        path_display = {
            shorten = {
                len = 4,
                exclude = {1, -1, -2}
            }
        },
    },

    pickers = {
        find_files = {
            find_command = { 'fd', '--type', 'f', '--strip-cwd-prefix', '-E', 'build', '-E', 'out' }
        },
        git_status = {
            git_icons = {
                added = " ",
                changed = " ",
                copied = " ",
                deleted = " ",
                renamed = "➡",
                unmerged = " ",
                untracked = " ",
            },

        }
    },
}
