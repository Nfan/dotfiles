BLACKOUT=420
LOCK=600
SLEEP=1200

HANDLER_NAME=handleEvent
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
E="$SCRIPT_DIR/$HANDLER_NAME"

swayidle \
  -w before-sleep "$E lock" \
  timeout "$(( BLACKOUT - 1 ))" "$E inhibit" \
  timeout "$BLACKOUT" "$E blackout" \
  resume "$E resume" \
  timeout "$(( LOCK - 1 ))" "$E inhibit" \
  timeout "$LOCK" "$E lock" \
  timeout "$(( SLEEP - 1 ))" "$E inhibit" \
  timeout "$SLEEP" "$E sleep"

pkill -9 "$HANDLER_NAME"
