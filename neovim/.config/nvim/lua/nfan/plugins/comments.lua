local leader = '<C-/>'

require("Comment").setup {
  toggler = {
    line = leader,
    block = leader..leader,
  },
  opleader = {
    line = leader,
    block = leader..leader,
  },
  extra = {
    above = leader..'O',
    below = leader..'o',
    eol = leader..'a',
  },
  ignore = '^$',
}
