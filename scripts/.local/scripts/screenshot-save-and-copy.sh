usage() {
  echo "usage: screenshot-save-and-copy <display|region|window> <path/to/save/dir>";
  echo "";
  echo "Saves a screenshot to the pointed directory";
  echo "display - fullscreen screenshot";
  echo "region - choose custom region with the mouse";
  echo "window - screenshot currently active window";
}


if [ $# -lt 2 ]; then
  usage;
  exit 1;
fi;


screenshot() {
  GEOMETRY="$1"
  SAVE_PATH="$2"
  grim -g "$GEOMETRY" - | tee "$SCREEN_PATH" | wl-copy;
}


screenshotOutput() {
  OUTPUT="$1"
  SAVE_PATH="$2"
  grim -o "$OUTPUT" - | tee "$SCREEN_PATH" | wl-copy;
}


TYPE="$1";
SCREEN_PATH="$2";
SCREEN_PATH="$SCREEN_PATH"/screenshot_$(date "+%F_%H-%M-%S_%N").png;

declare -r filter='
# returns the focused node by recursively traversing the node tree
def find_focused_node:
    if .focused then . else (if .nodes then (.nodes | .[] | find_focused_node) else empty end) end;
# returns a string in the format that grim expects
def format_rect:
    "\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height)";
find_focused_node | format_rect
'

NOTIFY_TITLE=""

case "$TYPE" in
  -h|--help)
    usage;
    exit 0;
    ;;
  region)
    screenshot "$(slurp -d)" "$SCREEN_PATH";
    NOTIFY_TITLE="Region Screenshot"
    ;;
  window)
		screenshot "$(swaymsg --type get_tree --raw | jq --raw-output "${filter}")" "$SCREEN_PATH";
    NOTIFY_TITLE="Window Screenshot"
    ;;
  *)
		screenshotOutput "$(swaymsg --type get_outputs --raw | jq --raw-output '.[] | select(.focused) | .name')" "$SCREEN_PATH";
    NOTIFY_TITLE="Screenshot"
    ;;
esac;

notify-send "$NOTIFY_TITLE" "Screenshot is copied and saved to:\n $SCREEN_PATH"
