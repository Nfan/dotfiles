WINDOW_SYSTEM := wayland
TERMINAL := alacritty
INSTALL_CMD ?= yay -S

all-config: $(WINDOW_SYSTEM)-config $(TERMINAL)-config
all-install: $(WINDOW_SYSTEM)-install $(TERMINAL)-install


alacritty-install: alacritty-config
	$(INSTALL_CMD) alacritty
alacritty-config:
	stow alacritty


wayland-config:
	stow \
		kanshi-display-profiles \
		mako \
		sway \
		fusuma

wayland-install: wayland-config fonts
	$(INSTALL_CMD) \
		sway \
		waybar \
		kanshi \
		swaylock \
		swayidle \
		mako \
		ruby-fusuma \
		brightnessctl

fonts:
	$(INSTALL_CMD) \
		ttf-jetbrains-mono \
		noto-fonts \
		noto-fonts-cjk \
		nerd-fonts-noto-sans-mono \
		nerd-fonts-noto-sans-regular-complete

console-tools-config:
	stow \
		bash \
		shell \
		gotop \
		htop \
		tig \
		tmux \
		mpv \
		neovim \
		ranger \
		sampler
