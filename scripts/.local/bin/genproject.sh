dirs=( "src" "include" "bin" "build" "external" "resources" )
for dir in ${dirs[@]}; do
  mkdir -p "$dir";
done

touch "CMakeLists.txt"

ln -sf build/compile_commands.json ./compile_commands.json
