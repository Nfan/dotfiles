# Requirements

- stow
  ```shell
  # ubuntu / debian
  apt-get install stow
  # arch
  pacman -S stow
  # and similar in another distributions
  ```

# Installation

```shell
cd ~
git clone git@gitlab.com:Nfan/dotfiles.git .dotfiles
stow <neovim>
```

Where *\<neovim\>* is one of the directories with its software configuration.
