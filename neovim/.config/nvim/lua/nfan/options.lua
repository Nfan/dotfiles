local opt = vim.opt

local set_tab_size = function(size)
  vim.opt.tabstop = size
  vim.opt.softtabstop = size
  vim.opt.shiftwidth = size
  vim.opt.expandtab = true
end

local set_tab_size_cmd = function(t)
  set_tab_size(tonumber(t.args))
end
vim.api.nvim_create_user_command('TabSize', set_tab_size_cmd, { nargs = 1 })

opt.timeoutlen = 500;
opt.showmode = false
opt.belloff = 'all'
opt.inccommand = 'nosplit'
opt.clipboard = 'unnamedplus' -- use system clipboard
opt.scrolloff = 10 -- there are always ten lines below cursor
opt.textwidth = 120
opt.colorcolumn = '150'
vim.g.mapleader = ' '

opt.number = true             -- show line numbers
opt.relativenumber = true     -- show line number relative to the current line

-- indentation
set_tab_size(2)
opt.autoindent = true
opt.cindent = true
opt.smartindent = true


-- long lines wrapping
opt.wrap = true         -- visually wrap lines that do not fit
opt.breakindent = true  -- indent wrapped liens
opt.showbreak = string.rep(" ", 3)
opt.linebreak = true    -- break at words


-- don't unload buffer after its window closed
-- so that its content could be used for completion
opt.hidden = true
opt.lazyredraw = true

opt.splitbelow = true -- split new windows below instead of above
opt.splitright = true -- split new windows right instead of left

opt.mouse = 'a' 	    -- mouse support
opt.mousefocus = true	-- focus window under the cursor


--completion menu
opt.completeopt = { 'menu', 'menuone', 'noselect' }
opt.wildmode = {'longest', 'list', 'full'}
opt.wildmenu = true
opt.wildignore = opt.wildignore + {
  '*.pyc',
  '*_build/*',
  '**/coverage/*',
  '**/node_modules/*',
  '**/.git/*'
}
opt.wildoptions = 'pum'
opt.pumheight = 20
-- opt.pumblend = 17  -- pop-up window transparancy
opt.signcolumn = 'yes'


-- search options
opt.ignorecase = true
opt.smartcase = true
opt.incsearch = true
opt.hlsearch = true

opt.showmatch = true  -- show briefly matching brace when new one inserted
opt.cursorline = true -- highlight cursor line
local group = vim.api.nvim_create_augroup("CursorLineControl", { clear = true })
local set_cursorline = function(event, value, pattern)
  vim.api.nvim_create_autocmd(event, {
    group = group,
    pattern = pattern,
    callback = function()
      vim.opt_local.cursorline = value
    end,
  })
end
set_cursorline("WinLeave", false)
set_cursorline("WinEnter", true)

opt.diffopt = { "internal", "filler", "closeoff", "hiddenoff", "algorithm:minimal" }

vim.diagnostic.config {
  update_in_insert = true
}
