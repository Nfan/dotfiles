#!/usr/bin/env bash

ANS=$(echo " Lock
 Sleep
 Restart
 Poweroff" | kickoff --from-stdin --stdout)

case "$ANS" in
  *Lock) swaylock -f;;
  *Sleep) systemctl suspend;;
  *Restart) systemctl reboot ;;
  *Poweroff) systemctl -i poweroff
esac
