local cmp = require('cmp')
local types = require('cmp.types')
local lspkind = require('lspkind')

lspkind.init()
require('nvim-autopairs').setup()
local cmp_autopairs = require('nvim-autopairs.completion.cmp')

local confirm_selection = cmp.mapping(cmp.mapping.confirm {
  behavior = cmp.ConfirmBehavior.Insert,
  select = true},
  { 'i', 'c' })

local function deprioritize_snippet(entry1, entry2)
  if entry1:get_kind() == types.lsp.CompletionItemKind.Snippet then
    vim.print(entry1.score, entry2.score)
    return entry1.score - 3 > entry2.score;
  end
  if entry2:get_kind() == types.lsp.CompletionItemKind.Snippet then
    return entry1.score > entry2.score - 3;
  end
end

cmp.setup {
  mapping = {
    ['<C-n>'] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
    ['<C-o>'] = function ()
      local entry = cmp.get_selected_entry()
      if entry ~= nil then
        vim.print(entry.score)
      end
    end,
    ['<C-p>'] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<C-y>'] = confirm_selection,

    ['<Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.confirm { select = true }
      else
        fallback()
      end
    end,
  },

  -- preselect = cmp.PreselectMode.None,

  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },

  sources = {
    { name = 'nvim_lua' },
    { name = 'luasnip' },
    { name = 'nvim_lsp' },
    { name = 'buffer', keyword_length = 3 },
    { name = 'path' },
  },

  sorting = {
    comparators = {
      deprioritize_snippet,
      cmp.config.compare.offset,
      cmp.config.compare.exact,
      cmp.config.compare.score,
      require('cmp-under-comparator').under,
      cmp.config.compare.kind,
      cmp.config.compare.sort_text,
      cmp.config.compare.length,
      cmp.config.compare.order,
    }
  },

  formatting = {
    format = lspkind.cmp_format {
      with_text = true,
      menu = {
        buffer = '[buf]',
        nvim_lsp = '[LSP]',
        nvim_lua = '[NVIM]',
        path = '[path]',
        luasnip = '[snip]'
      }
    }
  },

  experimental = {
    native_menu = false,
    ghost_text = true,
  }
}

cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())
