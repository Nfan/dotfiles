local window_handle = nil
local is_process_running = false

local close_window = function ()
  if window_handle ~= nil and vim.api.nvim_win_is_valid(window_handle) then
    vim.api.nvim_win_close(window_handle, false)
  end
end

local open_and_focus_scratch_buffer = function ()
  local scratch_buffer = vim.api.nvim_create_buf(false, true)
  if window_handle == nil or not vim.api.nvim_win_is_valid(window_handle) then
    vim.cmd [[split]]
    window_handle = vim.api.nvim_get_current_win()
    vim.api.nvim_win_set_height(window_handle, 10)
  end
  vim.api.nvim_win_set_buf(window_handle, scratch_buffer)
  vim.api.nvim_set_current_win(window_handle)
  vim.keymap.set('n', '<Esc>', close_window, { buffer = true })
end

local run_debug = function ()
  if is_process_running then
    vim.notify('Game is already running')
    return
  end
  open_and_focus_scratch_buffer()
  is_process_running = true
  vim.fn.termopen('godot -d', {
    on_exit = function ()
      is_process_running = false
    end
  })
end

vim.keymap.set('n', '<f6>', run_debug, { buffer = true })
