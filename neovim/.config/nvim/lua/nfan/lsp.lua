local nnoremap = require('nfan.keymap').nnoremap
local telescope_builtin = require('telescope.builtin')

local severities_prioritized = {
  vim.diagnostic.severity.ERROR,
  vim.diagnostic.severity.WARN,
  vim.diagnostic.severity.INFO,
  vim.diagnostic.severity.HINT,
}

local goto_diagnostic_prioritized = function (next)
  return function ()
    for _, severity in ipairs(severities_prioritized) do
      if #vim.diagnostic.get(0, { severity = severity }) ~= 0 then
        next({ severity = severity })
        return
      end
    end
  end
end

local M = {}

M.on_attach = function (_, buf)
  -- buf_nnoremap('K', vim.lsp.buf.hover)


  nnoremap('K', '<cmd>Lspsaga hover_doc<cr>', { buffer = buf })
  nnoremap('<leader>f', '<cmd>Lspsaga finder<cr>', { buffer = buf })
  nnoremap('<leader>a', '<cmd>Lspsaga code_action<cr>', { buffer = buf })
  -- nnoremap('<leader>a', vim.lsp.buf.code_action, { buffer = buf })
  nnoremap('<leader>o', '<cmd>Lspsaga outline<cr>', { buffer = buf })
  nnoremap('<leader>h', '<cmd>Lspsaga outgoing_calls<cr>', { buffer = buf })
  nnoremap('<leader>q', '<cmd>Lspsaga show_line_diagnostics<cr>', { buffer = buf })
  -- buf_nnoremap('<leader>n', '<cmd>Lspsaga diagnostic_jump_next<cr>')
  -- buf_nnoremap('<leader>N', '<cmd>Lspsaga diagnostic_jump_prev<cr>')
  -- go refactor
  nnoremap('grr', '<cmd>Lspsaga rename<cr>')

  -- buf_nnoremap('<leader>a', vim.lsp.buf.code_action)
  nnoremap('gr', telescope_builtin.lsp_references, { buffer = buf })
  nnoremap('gs', telescope_builtin.lsp_document_symbols, { buffer = buf })

  nnoremap('gd',
    function ()
      telescope_builtin.lsp_definitions({ jump_type = 'vsplit'})
    end,
    { buffer = buf })

  nnoremap('gt', vim.lsp.buf.type_definition, { buffer = buf }) -- Go to type definition of a symbol
  nnoremap('gi', vim.lsp.buf.implementation, { buffer = buf }) -- e.g. go to implementation of the interface method

  nnoremap('gn', goto_diagnostic_prioritized(vim.diagnostic.goto_next), { buffer = buf })
  nnoremap('gN', goto_diagnostic_prioritized(vim.diagnostic.goto_prev), { buffer = buf })

end

M.capabilities = require('cmp_nvim_lsp')
  .default_capabilities(vim.lsp.protocol.make_client_capabilities())
M.capabilities.textDocument.completion.completionItem.snippetSupport = true

return M
