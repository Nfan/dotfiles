#!/usr/bin/env bash
PADDING="$1"

is-stack-space() {
    [[ $(yabai -m query --spaces --space | jq -r .type) == "stack" ]]
}

set-space-padding() {
    local padding="$1"
    yabai -m config --space mouse top_padding $padding
    yabai -m config --space mouse bottom_padding $padding
    yabai -m config --space mouse left_padding $padding
    yabai -m config --space mouse right_padding $padding
}

NUM_WINDOWS=$(yabai -m query --windows --space mouse | jq -c '[.[] | select (."is-floating" == false and ."is-visible" == true)] | length')

if is-stack-space || [ "$NUM_WINDOWS" = "1" ] || [ "$NUM_WINDOWS" = "0" ]; then
    set-space-padding 0
else
    set-space-padding $PADDING
fi
